package com.techu.backend.model;


public class UserModel {

    private String id;
    private String name;
    private String segment;
    private Integer interest;
    private String expiryDate;

    // Method for Spring
    public UserModel(){
    }

    public UserModel(String id) {
        this.id = id;
    }

    public UserModel(String id, String name, String segment, Integer interest, String expiryDate) {
        this.id = id;
        this.name = name;
        this.segment = segment;
        this.interest = interest;
        this.expiryDate = expiryDate;
    }

    // Getter y Setter ID
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    // Getter y Setter Name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // Getter y Setter Segment
    public String getSegment() {
        return segment;
    }
    public void setSegment(String segment) {
        this.segment = segment;
    }

    // Getter y Setter Interest
    public Integer getInterest() {
        return interest;
    }
    public void setInterest(Integer interest) {
        this.interest = interest;
    }

    // Getter y Setter ExpiryDate
    public String getExpiryDate() {
        return expiryDate;
    }
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
}
