package com.techu.backend.controller;

import com.techu.backend.model.ProductModel;
import com.techu.backend.service.ProductMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins = "*")
public class ProductsController {

    @Autowired
    private ProductMongoService productService;

    @GetMapping("")
    public String home(){
        return "API REST Tech U! v2.0.0";
    }

    // GET all products (collection)
    @GetMapping("/products")
    public List<ProductModel> getProducts(){
        return productService.getProducts();
    }

    // GET product by id (instance)
    @GetMapping("/products/{id}")
    public ResponseEntity getProductById(@PathVariable String id){
        Optional<ProductModel> pr = productService.getProductById(id);
        if (pr.isPresent()){
            return new ResponseEntity(pr, HttpStatus.OK);
        }
        return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
    }

    // POST new product
    @PostMapping("/products")
    public ResponseEntity<String> postProduct(@RequestBody ProductModel newProduct){
        productService.addProduct(newProduct);
        return new ResponseEntity<>("Product created successfully", HttpStatus.CREATED);
    }

    // PUT for updating a product by ID
    @PutMapping("/products/{id}")
    public ResponseEntity putProductById(@PathVariable String id, @RequestBody ProductModel updatedProduct){
        Optional<ProductModel> pr = productService.getProductById(id);
        if (pr.isPresent()){
            productService.updateProductById(id, updatedProduct);
            return new ResponseEntity<>("Product was updated successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
    }

    // DELETE a product by ID
    @DeleteMapping("/products/{id}")
    public ResponseEntity deleteProductById(@PathVariable String id){
        Optional<ProductModel> pr = productService.getProductById(id);
        if (pr.isPresent()){
            productService.removeProductById(id);
            return new ResponseEntity<>("Product was removed successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
    }

}
