package com.techu.backend.model;

import org.apache.catalina.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "product")
public class ProductModel {

    @Id
    @NotNull
    private String id;
    private String name;
    private String description;
    private Integer period;
    private String photo;
    private String segment;
    private List<UserModel> users;
    private Integer numUsers;

    // Method for Spring
    public ProductModel(){
    }

    // Constructor
    public ProductModel(String id, String description, Integer period, String name, String photo,
                        String segment, List<UserModel> users, Integer numUsers) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.period = period;
        this.photo = photo;
        this.segment = segment;
        this.users = users;
        this.numUsers = numUsers;
    }

    // Getter y Setter ID
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    // Getter y Setter Name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // Getter y Setter Description
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // Getter y Setter Period
    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    // Getter y Setter Photo
    public String getPhoto() {
        return photo;
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSegment() {
        return segment;
    }
    public void setSegment(String segment) {
        this.segment = segment;
    }

    public List<UserModel> getUsers() {
        return users;
    }
    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    public Integer getNumUsers() {
        return numUsers;
    }
    public void setNumUsers(Integer numUsers) {
        this.numUsers = numUsers;
    }
}
