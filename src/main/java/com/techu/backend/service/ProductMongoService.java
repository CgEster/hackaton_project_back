package com.techu.backend.service;

import com.techu.backend.model.ProductModel;
import com.techu.backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class ProductMongoService {

    @Autowired
    ProductRepository productRepository;

    // READ
    public List<ProductModel> getProducts(){
        return productRepository.findAll();
    }

    // READ by ID
    public Optional<ProductModel> getProductById(String id){
        return productRepository.findById(id);
    }

    // CREATE
    public ProductModel addProduct(ProductModel newProduct){
        Random number = new Random();
        Integer newId = number.nextInt(100-0) + 0;
        while (productRepository.existsById(Integer.toString(newId))){
            newId = newId + 1;
        }
        newProduct.setId(Integer.toString(newId));
        if (newProduct.getUsers() != null ){
            newProduct.setNumUsers(newProduct.getUsers().size());
        }
        else{
            newProduct.setNumUsers(0);
        }
        return productRepository.save(newProduct);
    }

    // UPDATE product - PUT
    public void updateProductById(String id, ProductModel updatedProduct){
        Optional<ProductModel> productToUpdate = productRepository.findById(id);
        if (productToUpdate.isPresent()){
            if (productToUpdate.get().getUsers() != null){
                updatedProduct.setUsers(productToUpdate.get().getUsers());
                updatedProduct.setNumUsers(productToUpdate.get().getUsers().size());
                productRepository.save(updatedProduct);
            }
            else{
                updatedProduct.setNumUsers(0);
                productRepository.save(updatedProduct);
            }
        }
    }

    // DELETE by ID
    public boolean removeProductById(String id){
        Boolean exists = productRepository.existsById(id);
        if (exists) {
            productRepository.deleteById(id);
            return true;
        }
        else {
            return false;
        }
    }

}
